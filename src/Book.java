public class Book  extends LibraryItem {

    public enum Cover{
        HARD,
        PAPERBACK,
        LOOSE
    }

    private final Cover cover;

    public Book(String callNum, Cover cover) throws Exception {
        super(callNum);
        this.cover = cover;
    }

    @Override
    public String toString() {
        return "Book{" +
                super.toString() + ", cover=" + cover +
                '}';
    }
}
