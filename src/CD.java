public class CD extends LibraryItem {

    private final int diskInBox;

    public CD(String callNum, int diskInBox) throws Exception {
        super(callNum);
        this.diskInBox = diskInBox;
    }

    @Override
    public String toString() {
        return "CD{" +
                super.toString() + ", diskInBox=" + diskInBox +
                '}';
    }
}
