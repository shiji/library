public class DVD extends LibraryItem {

    public enum Encoding{
        PAL,
        NTSC
    }

    private final Encoding encoding;

    public DVD(String callNum, Encoding e) throws Exception {
        super(callNum);
        this.encoding = e;
    }

    @Override
    public String toString() {
        return "DVD{" +
                super.toString() + ", encoding=" + encoding +
                '}';
    }
}
