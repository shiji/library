import java.util.*;

public class Library {

    public final static int DEFAULT_LOAN_PERIOD = 7;

    private int loanPeriod;

    // Contains all items
    private Map<String, LibraryItem> items;

    // Contains books to be returned
    private List<LibraryItem> dropOffBox;


    public Library(){
        items = new HashMap<>();
        dropOffBox = new ArrayList<>();
        loanPeriod = DEFAULT_LOAN_PERIOD;
    }

    /**
     * Library with custom loan period
     * @param loan_period int
     */
    public Library(int loan_period){
        this();
        loanPeriod = loan_period;
    }

    /**
     * Add an item to the library
     * @param l item
     * @param <T> Any LibraryItem
     */
    public <T extends LibraryItem> void addItem(T l){
        items.put(l.getCallNum(), l);
    }

    /**
     * Add multiple items to the library
     * @param c Collection of LibraryItem
     */
    public void addItems(Collection<? extends LibraryItem> c){
        c.forEach(e -> items.put(e.getCallNum(), e));
    }

    /**
     * Remove an item from the library
     * @param l item
     * @param <T> Any LibraryItem
     */
    public <T extends LibraryItem> void removeItem(T l){
        items.remove(l.getCallNum());
    }

    /**
     * Remove multiple items to the library
     * @param c Collection of LibraryItem
     */
    public void removeItems(Collection<? extends LibraryItem> c){
        c.forEach(e -> items.remove(e.getCallNum(), e));
    }

    /**
     * Borrow an item from the library
     * @param l item
     * @param <T> Any LibraryItem
     * @return boolean success
     */
    public <T extends LibraryItem> boolean borrowItem(T l){
        if (l.canBeBorrowed()){
            return l.borrowItem(loanPeriod);
        }
        return false;
    }

    /**
     * Renew an item from the library
     * @param l item to be returned
     * @param <T> Any LibraryItem
     * @return boolean success
     */
    public <T extends LibraryItem> boolean renewItem(T l){
        if (l.canBeBorrowed()){
            return l.renewItem(loanPeriod);
        }
        return false;
    }


    /**
     * Return an item from the library
     * @param l item
     * @param <T> Any LibraryItem
     * @return boolean success
     */
    public <T extends LibraryItem> boolean returnItem(T l){
        return l.returnItem();
    }

    /**
     * Get loan period
     * @return int loan period
     */
    public int getLoanPeriod(){
        return loanPeriod;
    }
    /**
     * Process returns in the drop off box
     */
    public void clearDropOffBox(){
        dropOffBox.forEach(LibraryItem::returnItem);
        dropOffBox = new ArrayList<>();
    }

    /**
     * Drop off an item
     * @param l item
     * @param <T> Any LibraryItem
     */
    public <T extends LibraryItem> void dropOff(T l){
        dropOffBox.add(l);
    }

    /**
     * Get the total items in teh library
     * @return int total of items
     */
    public int size(){
        return items.size();
    }

    /**
     * Get the total items in teh library
     * @return int total of items
     */
    public int sizeAvailable(){
        return (int) items.values().stream().filter(LibraryItem::isAvailable).count();
    }
}
