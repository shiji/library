import java.time.LocalDate;
import java.util.Objects;

public abstract class LibraryItem {

    private String callNum;
    private boolean available;
    private LocalDate dueDate;

    public LibraryItem(String callNum) throws Exception {
        if (callNum == null || callNum.isEmpty() || callNum.isBlank()){
            throw new Exception("Call Number Must Not Be NULL/Blank");
        }
        this.callNum = callNum;
        available = true;
    }

    public String getCallNum() {
        return callNum;
    }

    public boolean canBeBorrowed(){
        return true;
    }

    /**
     * Borrow this item
     * @param days int loan period
     * @return boolean success
     */
    public synchronized boolean borrowItem(int days){
        if (available){
            available = false;
            dueDate = LocalDate.now().plusDays(days);
            System.out.printf("Borrowed: " + toString());
            return true;
        }
        return false;
    }

    /**
     * Return this item
     * @return boolean success
     */
    public synchronized boolean returnItem(){
        if (! available){
            if (dueDate.compareTo(LocalDate.now()) < 0){
                System.out.printf("Overdue: " + toString());
            }
            dueDate = null;
            available = true;
            System.out.printf("Returned: " + toString());
            return true;
        }
        return false;
    }

    /**
     * Renew this item
     * @param days int loan period
     * @return boolean success
     */
    public synchronized boolean renewItem(int days){
        return returnItem() && borrowItem(days);
    }

    /**
     * Book is available
     * @return boolean available
     */
    public boolean isAvailable(){
        return available;
    }

    /**
     * Get due date
     * @return LocalDate
     */
    public LocalDate getDueDate(){
        return dueDate;
    }


    @Override
    public String toString() {
        return  "callNum='" + callNum + '\'' +
                ", available=" + available +
                ", dueDate=" + dueDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LibraryItem that = (LibraryItem) o;
        return callNum.equals(that.callNum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(callNum);
    }

}
