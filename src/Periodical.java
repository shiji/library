public class Periodical extends LibraryItem {

    public enum Type{
        ACADEMIC,
        MAGAZINE,
        NEWSPAPER,
        OTHER
    }

    private final Type type;

    public Periodical(String callNum, Type t) throws Exception {
        super(callNum);
        this.type = t;
    }

    @Override
    public boolean canBeBorrowed(){
        return false;
    }

    @Override
    public String toString() {
        return "Periodical{" +
                super.toString() + ", type=" + type +
                '}';
    }
}
