import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


class LibraryTest {

    Library l;
    List<CD> cds;
    List<DVD> dvds;
    List<Book> books;
    List<Periodical> ps;

    @BeforeEach
    void setUp() throws Exception {
        l = new Library();

        cds = new ArrayList<>();
        cds.add(new CD("CTRC 0001" ,1));
        cds.add(new CD("CTRC 0002", 2));
        cds.add(new CD("CTRC 0003", 2));

        dvds = new ArrayList<>();
        dvds.add(new DVD("DVDV 0007", DVD.Encoding.PAL));
        dvds.add(new DVD("DVDV 0008", DVD.Encoding.PAL));
        dvds.add(new DVD("DVDV 0009", DVD.Encoding.NTSC));
        dvds.add(new DVD("DVDV 0010", DVD.Encoding.NTSC));

        books = new ArrayList<>();
        books.add(new Book("CSCP 0005", Book.Cover.HARD));
        books.add(new Book("CSCP 0006", Book.Cover.HARD));
        books.add(new Book("CSCP 0007", Book.Cover.PAPERBACK));
        books.add(new Book("CSCP 0008", Book.Cover.LOOSE));

        ps = new ArrayList<>();
        ps.add(new Periodical("PXYZ 5010", Periodical.Type.ACADEMIC));
        ps.add(new Periodical("PXYZ 5601", Periodical.Type.MAGAZINE));
        ps.add(new Periodical("PXYZ 5501", Periodical.Type.NEWSPAPER));
        ps.add(new Periodical("PXYZ 5011", Periodical.Type.ACADEMIC));
    }

    @AfterEach
    void tearDown() {
        l = new Library();
    }

    @Test
    void addItem() {
        assertEquals(l.size(), 0);
        l.addItem(cds.get(1));
        assertEquals(l.size(), 1);
    }

    @Test
    void addItems() {
        assertEquals(l.size(), 0);
        l.addItems(books);
        assertEquals(l.size(), 4);
        l.addItems(books);
        assertEquals(l.size(), 4);
        assertEquals(l.sizeAvailable(), 4);
    }

    @Test
    void removeItem() {
        l.addItems(books);
        assertEquals(l.size(), 4);
        l.removeItem(books.get(1));
        assertEquals(l.size(), 3);
        l.removeItem(books.get(1));
        assertEquals(l.size(), 3);
        l.removeItem(books.get(2));
        assertEquals(l.size(), 2);
    }

    @Test
    void removeItems() {
        l.addItems(books);
        assertEquals(l.size(), 4);
        l.addItems(dvds);
        assertEquals(l.size(), 8);
        l.removeItems(books);
        assertEquals(l.size(), 4);
        l.removeItems(books);
        assertEquals(l.size(), 4);
        l.removeItems(dvds);
        assertEquals(l.size(), 0);
    }

    @Test
    void borrowItem() {
        l.addItems(books);
        assertEquals(l.sizeAvailable(), 4);
        boolean result = l.borrowItem(books.get(1));
        assertTrue(result);
        assertEquals(l.sizeAvailable(), 3);
        result = l.borrowItem(books.get(1));
        assertFalse(result);
        assertEquals(l.sizeAvailable(), 3);
    }

    @Test
    void renewItem() {
        l.addItems(books);
        l.borrowItem(books.get(1));
        assertEquals(books.get(1).getDueDate(), LocalDate.now().plusDays(l.getLoanPeriod()));
        assertFalse(books.get(1).isAvailable());
    }

    @Test
    void returnItem() {
        l.addItems(books);
        l.borrowItem(books.get(1));
        assertFalse(books.get(1).isAvailable());
        assertEquals(l.sizeAvailable(), 3);
        l.returnItem(books.get(1));
        assertTrue(books.get(1).isAvailable());
        assertEquals(l.sizeAvailable(), 4);
    }

    @Test
    void clearDropOffBox() {
    }
}